PREFIX = $(DESTDIR)/usr
CC = gcc
CFLAGS= -Wall -Wextra -Werror
DEPS = igmptoo.h log.h
OBJ = igmptool.o log.o 

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

igmptool: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

clean:
	rm -f *.o igmptool

install:
	install -D -m 755 igmptool $(PREFIX)/sbin/igmptool
